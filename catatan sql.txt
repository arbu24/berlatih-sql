C:/xampp/mysql/bin> mysql -u root -p

syntax SQL
///
show databases;

///
create database <nama_database>;

///
use database <nama_database>;

///
show tables;

///
create table <nama_table> (
	nama_colom <type_data> <constraints>,
	nama_colom <type_data> <constraints>,
	nama_colom <type_data> <constraints>,
	);

//type_data
int
decimal
char
varchar
text
date
datetime
timstamp

//constraints
not null
primary key
auto_increment
unique

create table if not exists <nama_table>(
	nama_colom <type_data> <constraints>,
	nama_colom <type_data> <constraints>,
	nama_colom <type_data> <constraints>,
	);

///
describe <nama_table>;

///tambah data pada table
insert into <nama_table> values ();

//nambah beberapa data
insert into <nama_table> (<nama_colom1>,<nama_colom2>)
values('isi_kolom1','isi_kolom2'), ('isi_kolom1',isi_kolom2);

///menampilkan data
//menampilkan semua data pada table yang di pilih
select * from <nama_table>;
select <nama_kolom1>,<nama_kolom2> from <nama_table>;

//dengan kondisi
select * from <nama_table>
where <nama_kolom> = "<something>";

//menampilkan data dari 2 table
// gunakan join
	inner join
	left join	
	right join
	full outer join

select <a.nama_kolom>, ... , <b.nama_kolom>
from <nama_table_a> as a
join <nama_table_b> as b  // ubah join sesuai kebutuhan 
on a.<foreign_key_b> = b.<primary_key>;
//tambahan kondisi
where ......       
order by <nama_kolom>      // pengurutan data berdasarkan <nama_kolom> default asc


///update data
update <nama_table>
set <nama_kolom1> = "isi1", <nama_kolom2> = "isi2"
where <kondisi> ;   //cth where id = "2";


///menghapus data

delete from <nama_table> where <kondisi>;

